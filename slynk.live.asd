(asdf:defsystem #:slynk.live
  :description "Swank.live, but for Sly instead of SLIME"
  :author "original: Chris Bagley (Baggers) <techsnuffle@gmail.com>; edited for use with Sly by bilegeek"
  :license "BSD 2 Clause"
  :serial t
  :depends-on (#:slynk)
  :components ((:file "package")
               (:file "slynk.live")))
