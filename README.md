Original Baggers README, but replaced swank with slynk.
-------------------------------------------------------

Originally from: https://github.com/cbaggers/swank.live

Some helpers that make livecoding at little easier with slime/swank

Exposes:

# #'update-slynk

Call this function to get slynk to handle requests. Put this in your main game/demo/etc loop to keep the repl live whist your code runs.

# #'peek

A quick way of calling #'slynk:inspect-in-emacs
